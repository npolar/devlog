---
categories: development
title: Remote team programming
date: 2021-01-16
author: Mikhail (@mitkin)
tags:
- dev
---

This post gives some ideas what tools are out there for team programming. By team programming I mean the same thing as pair programming but available to more than two people. Maybe ["mob programming"](https://en.wikipedia.org/wiki/Mob_programming) is a more familiar term.

<!--more-->

## Requirements
It should be free and open source. It must be based on common and easily available to everybody on the team.
Below is the options that would be on top of my list:

### Vim + Tmux
[Read Pauls post on how to set up tmux for shared sessions](https://ptc-it.de/pairing-with-tmux-and-vim/)

Vim and tmux are some of the frequently used tools for team programming. 
Facilitating user must log into the server (e.g. via ssh) and start a tmux session and share the name of the session name with the others.
Other users must connect into the same server and connect to the same tmux session. Whatever is launched from within
tmux gets available to the rest of the team.

* Pros: vim (or other terminal based editors) and tmux are commonly used tools and easy to install.
* Cons: team  must have access to the common server.

### Vscode Live Share
Visual studio code is another popular editor with many capabilities. Microsoft released LiveShare plugin for collaborative writing.
LiveShare includes more than possibility to write code together, it also provides audio calls.

The getting started process looks like this:

* Facilitating user must install LiveShare extension for VSCode. 
* The user must invoke "Start collaboration session" command.
* New server room will be created and link copied to clipboard automatically. Link can be shared with the rest of the team.
* The others also need to have the LiveShare plugin installed. Then they should invoke "Join collaboration session" command and use provided link.

* Pros: Popular and fast editor, LiveShare plugin comes with bells and whistles. Does not need own server.
* Cons: Runs on cloud server which is not ideal when access to local premises are required.

### VSCode + GitDuck
GitDuck is an extension similar to LiveShare, except it also allows videoconferencing. Author has not tried it out yet.

### Atom + Teletype
Teletype extension is similar to LiveShare, but does one thing: allows collaborative writing. Users must install Teletype extension for Atom editor
and create a channel. It will require sighning into github for creating a token and using it for Teletype.

* Pros: It works
* Cons: Requires github account. 

### Sublime + RemoteCollab
Author has not tried out the tool yet. Project seems to be abandoned in 2014.

### Upterm
Upterm is a command line tool for creating remote server on upterm.dev that could be used with tmux etcetera. Author has not tried it out yet.

### Mob: Swift Git Handover 
[Swift Git Handover](https://github.com/remotemobprogramming/mob)
