---
categories: development
title: Using Docker gitlab-runner with Docker executor
date: 2021-03-19
tags:
- gitlab
- docker
- ci/cd
author: Mikhail (@mitkin)
---

You can execute jobs outlined in `.gitlab-ci.yml` file on your local machine using `gitlab-runner`. It won't run the entire pipeline, that task is done by gitlab server side software, but you can execute individual jobs. Let's see what is the quickiest way to get gitlab-runner execute your jobs.

<!--more--> 

## Requirements
You need a machine with docker service installed and running. On Ubuntu install `docker.io` package.

## Obtain Docker image with gitlab-runner 
Execute run command to obtain the image with gitlab-runner and run the container
```
docker run \
   -d --rm \
   --name my-awesome-container \
   -v $PWD:$PWD \
   -v /var/run/docker.sock:/var/run/docker.sock \
   gitlab/gitlab-runner:latest
```

Useful to know optins and arguments:
* `--name` - by what name to refer to the container
* `-v $PWD:$PWD` - mount your current directory $PWD to exact same path inside the container
* `-v /var/run/docker.sock:/var/run/docker.sock` - address through which to connect to docker daemon
* `gitlab/gitlab-runner:latest` - which image we are going to be using 

## Executing the runner
Let's assume you have already cloned the git project to your local machine. Your project should contain CI file called `.gitlab-ci.yml`. The pipeline will typically execute all jobs grouped by stages. That part is coordinated by the server side gitlab software. Your local `gitlab-runner` will execute only individual jobs. 

Mind that gitlab-runner will execute only commited changes to your code. Only changes to `.gitlab-ci.yml` need not be commited, but for everything gitlab-runner will ignore uncommited changes.

To execute the runner do the following:
```
docker exec \
  -it -w $PWD my-awesome-container gitlab-runner docker \
  --env FOO=BAR \
  job-name
```

Usefule to understand parameters:
* `my-awesome-container` - container from the `gitlab/gitlab-runner:latest` image
* `--env FOO=BAR` - environment variable that you want to pass to the job
* `job-name` - the name of the job to be executed, as in .gitlab-ci.yml file
