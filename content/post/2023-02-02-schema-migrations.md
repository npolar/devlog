---
categories: development
title: Database schema migrations
date: 2023-02-02
author: Mikhail (@mitkin)
tags:
- dev
- database
---

# Schema migrations

When the expected structure of the database changes then DB schema in persistent environment needs to be modified.
Typically an `upgrage` and `downgrade` methods are added. The former is bringing DB to a expected structure and the latter is used for rollbacks in case something goes wrong.


This reddit discussion has some good links what tools could be used for schema migrations: https://www.reddit.com/r/PostgreSQL/comments/u5qkbr/what_migrationversioning_tool_do_you_use/

Some of them are:

Go:
- [tern](https://github.com/jackc/tern)
- [goose](https://github.com/pressly/goose)

Python
- [alembic](https://github.com/pressly/goose)
