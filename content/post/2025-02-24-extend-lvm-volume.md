If you added a new disc to a VM in vcenter, and want to extend the existing volume with it, here is the recipe:

```
#!/bin/bash

# Variables
VG_NAME="ubuntu-vg"
LV_NAME="ubuntu-lv"
NEW_DISK="/dev/sdb"

# Check if the disk exists
if ! lsblk | grep -q "$(basename $NEW_DISK)"; then
    echo "Error: Disk $NEW_DISK not found!"
    exit 1
fi

# Create a Physical Volume
sudo pvcreate $NEW_DISK
if [ $? -ne 0 ]; then
    echo "Error: Failed to create physical volume on $NEW_DISK"
    exit 1
fi

echo "$NEW_DISK successfully initialized as a physical volume."

# Extend the Volume Group
sudo vgextend $VG_NAME $NEW_DISK
if [ $? -ne 0 ]; then
    echo "Error: Failed to extend volume group $VG_NAME"
    exit 1
fi

echo "Volume group $VG_NAME extended with $NEW_DISK."

# Extend the Logical Volume
sudo lvextend -l +100%FREE /dev/$VG_NAME/$LV_NAME
if [ $? -ne 0 ]; then
    echo "Error: Failed to extend logical volume $LV_NAME"
    exit 1
fi

echo "Logical volume $LV_NAME extended."

# Resize the Filesystem
FS_TYPE=$(df -T | grep "/dev/$VG_NAME/$LV_NAME" | awk '{print $2}')

if [ "$FS_TYPE" == "ext4" ]; then
    sudo resize2fs /dev/$VG_NAME/$LV_NAME
elif [ "$FS_TYPE" == "xfs" ]; then
    sudo xfs_growfs /dev/$VG_NAME/$LV_NAME
else
    echo "Error: Unsupported filesystem type $FS_TYPE"
    exit 1
fi

echo "Filesystem on $LV_NAME resized successfully."

echo "LVM extension completed successfully!"
```